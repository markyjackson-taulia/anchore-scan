# Bitbucket Pipelines Pipe: Anchore Container Scan

The Anchore Container Scan pipe can be called to perform an in-depth analysis of a container image, resulting in reports showing software package vulnerabilities, Dockerfile and other security/compliance violations associated with the container image.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: anchore/scan-pipe
  variables:
    IMAGE_NAME: "<string>"
    # DEBUG: "<boolean>" # Optional
    # VULN_REPORT_SEVERITY_CUTOFF: "<string>" # Optional
    # CUSTOM_POLICY_PATH: '<string>' # Optional
    # ENABLE_VULN_REPORT: '<boolean>' # Optional
    # ENABLE_POLICY_REPORT: '<boolean>' # Optional
```
## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| IMAGE_NAME (*)        | The name of the container image to be scanned. |
| DEBUG                 | Turn on extra debug information. Default: `false`. |
| VULN_REPORT_SEVERITY_CUTOFF       | Lowest severity level (LOW, MEDIUM, HIGH, CRITICAL) for a finding that will result in a 'fail' status in the Anchore Vulnerability Report. |
| CUSTOM_POLICY_PATH    | Relative path in your clone directory to a custom Anchore Policy Bundle (JSON).  See Anchore documentation at http://docs.anchore.com for more information on creating a custom Anchore policy. |
| ENABLE_VULN_REPORT    | Enable generation of vulnerability report. Defaut: 'true'. |
| ENABLE_POLICY_REPORT  | Enable generation of Anchore policy evaluation report. Defaut: 'true'. |

_(*) = required variable._

## Prerequisites

## Examples

Basic example:

```yaml
script:
  - export IMAGE_NAME=your_container_repo/your_container_image:$BITBUCKET_COMMIT

  # build the Docker image (this will use the Dockerfile in the root of the repo)
  - docker build -t $IMAGE_NAME -f Dockerfile .

  # run the anchore scan pipe
  - pipe: docker://anchore/scan-pipe
  variables:
    IMAGE_NAME: $IMAGE_NAME

  # push the new Docker image to the Docker registry
  - docker login --username $DOCKER_HUB_USERNAME --password $DOCKER_HUB_PASSWORD  
  - docker push $IMAGE_NAME

```

Advanced example:

```yaml
script:
  - export IMAGE_NAME=your_container_repo/your_container_image:$BITBUCKET_COMMIT

  # build the Docker image (this will use the Dockerfile in the root of the repo)
  - docker build -t $IMAGE_NAME -f Dockerfile .

  # run the anchore scan pipe
  - pipe: docker://anchore/scan-pipe
  variables:
    IMAGE_NAME: $IMAGE_NAME
    DEBUG: "true"
    VULN_REPORT_SEVERITY_CUTOFF: "MEDIUM"
    
  # push the new Docker image to the Docker registry
  - docker login --username $DOCKER_HUB_USERNAME --password $DOCKER_HUB_PASSWORD  
  - docker push $IMAGE_NAME

```

## Support

If you’d like help with this pipe, or you have an issue or feature request, let us know.
The pipe is maintained by Anchore Inc. You can contact us by joining our community slack channel directly https://anchore.com/slack

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages when running the pipe with DEBUG enabled
- steps to reproduce
